import 'package:demo_flutter_viper/interactor/user_interactor.dart';
import 'package:demo_flutter_viper/router/login_router.dart';
import 'package:flutter/material.dart';

abstract class LoginContract{
  void onLoginSuccess(){}
  void onLoginError(){}
  void onLoginFailt(){}
}

class LoginPresenter{
  LoginContract _view;
  UserInteractor _model;
  LoginRouter _router;

  LoginPresenter(this._view){
    _model = UserInteractor();
    _router = LoginRouter();
  }

  void login (String account, String password){
    _model.login(account, password).then((data){
      if(data == 1){
        _view.onLoginSuccess();
      }
      else{
        _view.onLoginFailt();
      }
    }).catchError((onError){
      _view.onLoginError();
    });
  }

  void navigationHome(BuildContext context){
    _router.navigationHome(context);
  }
}