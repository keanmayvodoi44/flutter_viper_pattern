import 'package:demo_flutter_viper/entity/images.dart';
import 'package:demo_flutter_viper/interactor/image_interactor.dart';

abstract class HomeContract{
  void onLoadComplete(List<Images> items){}
  void onLoadError(){}
}

class HomePresenter{
  HomeContract _view;
  ImageInteractor _model;

  HomePresenter(this._view){
    _model = ImageInteractor();
  }

  void fetchImages(){
    _model.fetch().then((data){
      _view.onLoadComplete(data);
    }).catchError((onError){
      _view.onLoadError();
    });
  }
}