import 'package:demo_flutter_viper/view/home.dart';
import 'package:flutter/material.dart';

class LoginRouter{

  void navigationHome(BuildContext context){
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => Home()
        )
    );
  }
}