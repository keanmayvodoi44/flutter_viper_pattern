class Images{
  int id;
  String url;

  Images({this.id, this.url});

  factory Images.fromJson(Map<String, dynamic> json){
    return Images(
        id: int.parse(json['Id']),
        url: json['Url']
    );
  }
}